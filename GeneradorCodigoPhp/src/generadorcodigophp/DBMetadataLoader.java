/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generadorcodigophp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author gabrielguevara
 */
public class DBMetadataLoader {
    
    private ArrayList<String> tables = new ArrayList<>();
    private Map<String, List<String>> fieldsPerTable = new HashMap<>();
    private Map<String, List<String>> kindsPerTable = new HashMap<>();
    
    private DataSource dataSource;

    public DBMetadataLoader() {
        dataSource = DataSourceFactory.getMySQLDataSource();
    }
    
    public List<String> refreshTables() throws SQLException {
        Connection connection = dataSource.getConnection();
        String schema = connection.getSchema();
        String[] tableType = {"TABLE"};
        ResultSet rs = null;
        rs = connection.getMetaData().getTables(null, schema, null, tableType);
        while(rs.next()){
                tables.add(rs.getString(3));
        }
        if(rs != null) rs.close();
        if(connection != null) connection.close();
        return tables;
    }

    public List<String> refreshFieldsForTable(String table) throws SQLException {
        Connection connection = dataSource.getConnection();
        ResultSet rs = null;
        rs = connection.getMetaData().getColumns(null, null, table, null);
        List<String> columns = new ArrayList<>();
        List<String> kinds = new ArrayList<>();
        while(rs.next()){
            columns.add(rs.getString(4));
            kinds.add(rs.getString(6));
        }
        fieldsPerTable.put(table, columns);
        kindsPerTable.put(table, kinds);
        if(rs != null) rs.close();
        if(connection != null) connection.close();
        return fieldsPerTable.get(table);
    }
    
    public ArrayList<String> getTables() {
        return tables;
    }

    public void setTables(ArrayList<String> tables) {
        this.tables = tables;
    }
    
    public List<String> getKindsPerTableName(String tableName) {
        return kindsPerTable.get(tableName);
    }
    
    public String getKindsPerTableNameAsParamDescription(String tableName) {
        String paramDescription = "";
        for (String kindOfField : kindsPerTable.get(tableName)) {
            switch (kindOfField) {
                
                case "INT": 
                    paramDescription += 'i';
                    break;
                    
                case "VARCHAR": 
                    paramDescription += 's';
                    break;
                    
                case "DOUBLE": 
                    paramDescription += 'd';
                    break;
                    
                case "FLOAT": 
                    paramDescription += 'd';
                    break;
                    
                case "DATETIME": 
                    paramDescription += 's';
                    break;
                    
                case "DATE": 
                    paramDescription += 's';
                    break;
                    
                case "TIME": 
                    paramDescription += 's';
                    break;
                
                default:
                    paramDescription += 'd';
                    break;
                    
            }
        }
        return paramDescription;
    }

    public Map<String, List<String>> getFieldsPerTable() {
        return fieldsPerTable;
    }
    
}
