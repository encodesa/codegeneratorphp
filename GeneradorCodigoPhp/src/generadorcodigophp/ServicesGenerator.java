/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generadorcodigophp;

import java.util.List;

/**
 *
 * @author gabrielguevara
 */
public class ServicesGenerator {
    
    private String className;
    private String classNameCapital;
    private String classNameUpperCamel;
    private String classNameLowerCamel;
    private String requiredPropertieInsert;
    private String requiredPropertieUpdate;
    
    private String requiredPropertieTemplate = "isset($objArr['%requiredpropertie%'])";
    
    private String template = "<?php\n" +
"\n" +
"/* \n" +
" * To change this license header, choose License Headers in Project Properties.\n" +
" * To change this template file, choose Tools | Templates\n" +
" * and open the template in the editor.\n" +
" */\n" +
"class %classname% extends API {\n" +
"    \n" +
"    public $_apiName = '%tablauppercamel%';\n" +
"    \n" +
"    protected function get() {     \n" +
"        $token = $this->getBearerToken();\n" +
"        try {\n" +
"            if ( CheckAPIPermissions::haveReadPermission($this->_apiName, $token) ) {\n" +
"                $_id = $this->getId(); //false si no existe o no es numero, numero si existe\n" +
"                if($_id !== false){ //muestra 1 solo registro si es que existiera ID                 \n" +
"                    $%tablalowercamel%DAO = new %tablauppercamel%DAO();\n" +
"                    echo json_encode( $%tablalowercamel%DAO->find%tablauppercamel%ById($_id), JSON_PRETTY_PRINT);\n" +
"                 }else{ //muestra todos los registros                   \n" +
"                    $%tablalowercamel%DAO = new %tablauppercamel%DAO();\n" +
"                    echo json_encode( $%tablalowercamel%DAO->find%tablauppercamel%All(), JSON_PRETTY_PRINT);\n" +
"                }\n" +
"            } else {\n" +
"                $this->response(401,\"Unauthorized\",\"No posee los permisos necesarios para realizar esta operación\");\n" +
"            }\n" +
"        } catch (Exception $e) {\n" +
"            $this->response(401, \"Unauthorized\", 'Error de autenticación ' . $e->getMessage());\n" +
"        }\n" +
"    }\n" +
"\n" +
"    protected function post() {\n" +
"        $token = $this->getBearerToken();\n" +
"        try {\n" +
"            if ( CheckAPIPermissions::haveWritePermission($this->_apiName, $token) ) {\n" +
"                $objArr = (array)$this->getInformacion();\n" +
"                if (empty($objArr)){\n" +
"                     $this->response(422,\"error\",\"Nothing to add. Check json\");                           \n" +
"                }else if(%requiredpropertiesinsert%){\n" +
"                    $%tablalowercamel% = new %tablauppercamel%DTO();\n" +
"                    $%tablalowercamel%->setDataFromArray($objArr);\n" +
"                    $%tablalowercamel%DAO = new %tablauppercamel%DAO();\n" +
"                    $respuesta = $%tablalowercamel%DAO->insertar%tablauppercamel%($%tablalowercamel%);\n" +
"                    $this->response(200,\"success\", $respuesta);                             \n" +
"                }else{\n" +
"                    $this->response(422,\"error\",\"The property is not defined\");\n" +
"                }\n" +
"            } else {\n" +
"                $this->response(401,\"Unauthorized\",\"No posee los permisos necesarios para realizar esta operación\");\n" +
"            }\n" +
"        } catch (Exception $e) {\n" +
"            $this->response(401, \"Unauthorized\", 'Error de autenticación ' . $e->getMessage());\n" +
"        }\n" +
"    }\n" +
"\n" +
"    protected function put() {   \n" +
"        $token = $this->getBearerToken();\n" +
"        try {\n" +
"            if ( CheckAPIPermissions::haveUpdatePermission($this->_apiName, $token) ) {\n" +
"                $_id = $this->getId(); //false si no existe o no es numero, numero si existe\n" +
"                if($_id !== false){ //muestra 1 solo registro si es que existiera ID                 \n" +
"                    $objArr = $this->getInformacion();\n" +
"                    if (empty($objArr)){\n" +
"                        $this->response(422,\"error\",\"Nothing to add. Check json\");                           \n" +
"                    }else if(%requiredpropertiesupdate%){\n" +
"                        $%tablalowercamel% = new %tablauppercamel%DTO();\n" +
"                        $%tablalowercamel%->setDataFromArray($objArr);\n" +
"                        $%tablalowercamel%DAO = new %tablauppercamel%DAO();\n" +
"                        $%tablalowercamel%DAO->actualizar%tablauppercamel%($%tablalowercamel%, $_id);\n" +
"                        $this->response(200,\"success\",\"record updated\");                             \n" +
"                    }else{\n" +
"                        $this->response(422,\"error\",\"The property is not defined\");\n" +
"                    }\n" +
"                    exit;\n" +
"                }\n" +
"                $this->response(400);\n" +
"            } else {\n" +
"                $this->response(401,\"Unauthorized\",\"No posee los permisos necesarios para realizar esta operación\");\n" +
"            }\n" +
"        } catch (Exception $e) {\n" +
"            $this->response(401, \"Unauthorized\", 'Error de autenticación ' . $e->getMessage());\n" +
"        }\n" +
"    }\n" +
"    \n" +
"    protected function delete() {\n" +
"        $token = $this->getBearerToken();\n" +
"        try {\n" +
"            if ( CheckAPIPermissions::haveDeletePermission($this->_apiName, $token) ) {\n" +
"                $_id = $this->getId(); //false si no existe o no es numero, numero si existe\n" +
"                if($_id !== false){ //muestra 1 solo registro si es que existiera ID                 \n" +
"                    $%tablalowercamel%DAO = new %tablauppercamel%DAO();\n" +
"                    $%tablalowercamel%DAO->delete($_id);\n" +
"                    $this->response(204);                   \n" +
"                    exit;\n" +
"                }\n" +
"                $this->response(400);\n" +
"            } else {\n" +
"                $this->response(401,\"Unauthorized\",\"No posee los permisos necesarios para realizar esta operación\");\n" +
"            }\n" +
"        } catch (Exception $e) {\n" +
"            $this->response(401, \"Unauthorized\", 'Error de autenticación ' . $e->getMessage());\n" +
"        }\n" +
"    }\n" +
"}";
    
    public void setClassName(String className) {
        this.className = PropertieTranslator.createUpperCamelCase(className) + "Services";
        this.classNameUpperCamel = PropertieTranslator.createUpperCamelCase(className);
        this.classNameLowerCamel = PropertieTranslator.createLowerCamelCase(className);
        this.classNameCapital = className.toUpperCase();
    }
    
    public void generateRequiredPropertiesUpdate(List<String> requiredProperties) {
        String[] fields = requiredProperties.toArray(new String[0]);
        generateRequiredPropertiesUpdate(fields);
    }
    
    public void generateRequiredPropertiesInsert(List<String> requiredProperties) {
        String[] fields = requiredProperties.toArray(new String[0]);
        generateRequiredPropertiesInsert(fields);
    }
    
    public void generateRequiredPropertiesUpdate(String[] requiredProperties) {
        this.requiredPropertieUpdate = generateRequiredProperties(requiredProperties);
    }
    
    public void generateRequiredPropertiesInsert(String[] requiredProperties) {
        this.requiredPropertieInsert = generateRequiredProperties(requiredProperties);
    }
    
    private String generateRequiredProperties(String[] requiredProperties) {
        String condition = "";
        boolean isFirst = true;
        for (String requiredPropertie : requiredProperties) {
            String conditionSegment = requiredPropertieTemplate.replace("%requiredpropertie%", requiredPropertie);
            if (isFirst) {
                condition = conditionSegment;
            } else {
                condition += " && " + conditionSegment;
            }
            isFirst = false;
        }
        return condition;
    }
    
    public String replaceValues() {
        template = template.replace("%classname%", className);
        template = template.replaceAll("%tablauppercamel%", classNameUpperCamel);
        template = template.replaceAll("%tablalowercamel%", classNameLowerCamel);
        template = template.replace("%requiredpropertiesinsert%", requiredPropertieInsert);
        template = template.replace("%requiredpropertiesupdate%", requiredPropertieUpdate);
        return template;
    }
    
    public String generateServiceClass(String tabla, List<String> requiredPropertieInsert, List<String> requiredPropertieUpdate) {
        String[] insertRequieredProperties = requiredPropertieInsert.toArray(new String[0]);
        String[] updateRequieredProperties = requiredPropertieUpdate.toArray(new String[0]);
        return generateServiceClass(tabla, insertRequieredProperties, updateRequieredProperties);
    }
    
    public String generateServiceClass(String tabla, String[] requiredPropertieInsert, String[] requiredPropertieUpdate) {
        this.setClassName(tabla);
        this.generateRequiredPropertiesInsert(requiredPropertieInsert);
        this.generateRequiredPropertiesUpdate(requiredPropertieInsert);
        return this.replaceValues();
    }

    public String getClassName() {
        return className;
    }
    
}
