/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generadorcodigophp;

import com.mysql.jdbc.DatabaseMetaData;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;
/**
 *
 * @author gabrielguevara
 */
public class GeneradorCodigoPhp {

    private static String rutaDTO = "/Users/gabrielguevara/Desktop/ClasesGeneradas/DTO";
    private static String rutaDAO = "/Users/gabrielguevara/Desktop/ClasesGeneradas/DAO";
    private static String rutaServices = "/Users/gabrielguevara/Desktop/ClasesGeneradas/Services";
    private static String rutaController = "/Users/gabrielguevara/Desktop/ClasesGeneradas/Controller";
    private static String rutaDefinicionTabla = "/Users/gabrielguevara/Desktop/ClasesGeneradas/DefinicionTabla";
    private static String rutaDefinicionCamposAPI = "/Users/gabrielguevara/Desktop/ClasesGeneradas/DefinicionCamposAPI";
    private static String rutaDefinicionCamposTabla = "/Users/gabrielguevara/Desktop/ClasesGeneradas/DefinicionCamposTabla";
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DBMetadataLoader dbLoader = new DBMetadataLoader();
        try {
            List<String> tablas = dbLoader.refreshTables();
            
            generateNombreTablas(tablas);
            
            //System.out.println(generarTruncateRestAUI(tablas));
            
            for (String tabla : tablas) {
                List<String> camposInsert = dbLoader.refreshFieldsForTable(tabla);
                List<String> camposUpdate = new ArrayList<>(camposInsert);
                String paramsDescriptionInsert = dbLoader.getKindsPerTableNameAsParamDescription(tabla);
                String paramsDescriptionUpdate = new String(paramsDescriptionInsert);
                
                if (camposUpdate.size() > 1) {
                    camposUpdate.remove(0);
                    paramsDescriptionUpdate = paramsDescriptionUpdate.substring(1);
                }
                
                generateDTOFile(tabla, camposInsert);
                
                generateDAOFile(tabla, camposInsert, paramsDescriptionInsert, camposUpdate, paramsDescriptionUpdate);
                
                List<String> requiredFields = new ArrayList<String>();
                requiredFields.add("id");
                generateService(tabla, requiredFields, requiredFields);
                
                generateControllerFile(tabla);
                
            }
            
            generateCamposTabla(dbLoader.getFieldsPerTable());
            generateCamposAPI(dbLoader.getFieldsPerTable());
            
        } catch (SQLException ex) {
            Logger.getLogger(GeneradorCodigoPhp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private static String generarTruncateRestAUI(List<String> tablas) {
        String query = "";
        String template = "TRUNCATE TABLE %TABLA%;\n" +
"ALTER TABLE %TABLA% AUTO_INCREMENT = 1;\n";
        for (String tabla : tablas) {
            query += "\n" + template.replaceAll("%TABLA%", tabla);
        }
        return query;
    }
    
    private static void generateDTOFile(String tabla, List<String> campos) {
        DTOGenerator dTOGenerator = new DTOGenerator();
        String dtoClass = dTOGenerator.generateDTOClass(tabla, campos);
        writeToFile(rutaDTO, dTOGenerator.getClassName(), "php", dtoClass);
    }
    
    private static void generateDAOFile(String tabla, List<String> camposInsert, String paramsDecriptionInsert, List<String> camposUpdate, String paramsDecriptionUpdate) {
        DAOGenerator dAOGenerator = new DAOGenerator();
        String daoClass = dAOGenerator.generateDAOClass(tabla, camposInsert, paramsDecriptionInsert, camposUpdate, paramsDecriptionUpdate);
        writeToFile(rutaDAO, dAOGenerator.getClassName(), "php", daoClass);
    }
    
    private static void generateService(String tabla, List<String> requiredPropsInsert, List<String> requiredPropsUpdate) {
        ServicesGenerator servicesGenerator = new ServicesGenerator();
        String serviceClass = servicesGenerator.generateServiceClass(tabla, requiredPropsInsert, requiredPropsUpdate);
        writeToFile(rutaServices, servicesGenerator.getClassName(), "php", serviceClass);
    }
    
    private static void generateCamposAPI(Map<String, List<String>> contenido) {
        CampoApiGenerator campoApiGenerator = new CampoApiGenerator();
        campoApiGenerator.generarContenidoDesdeLista(contenido);
        writeToFile(rutaDefinicionCamposAPI, "CamposApis", "php", campoApiGenerator.generateClass());
    }
    
    private static void generateCamposTabla(Map<String, List<String>> contenido) {
        CampoTablaGenerator campoTablaGenerator = new CampoTablaGenerator();
        campoTablaGenerator.generarContenidoDesdeLista(contenido);
        writeToFile(rutaDefinicionCamposTabla, "CamposTablas", "php", campoTablaGenerator.generateClass());
    }
    
    private static void generateNombreTablas(List<String> tablas) {
        TableNameGenerator tableNameGenerator = new TableNameGenerator();
        tableNameGenerator.generarContenidoDesdeLista(tablas);
        writeToFile(rutaDefinicionTabla, "NombreTabla", "php", tableNameGenerator.generateClass());
    }
    
    private static void generateControllerFile(String tabla) {
        ControllerGenerator controllerGenerator = new ControllerGenerator();
        controllerGenerator.setClassName(tabla);
        writeToFile(rutaController, controllerGenerator.getClassName(), "php", controllerGenerator.generateController());
    }
    
    // Save to file Utility
    private static void writeToFile(String path, String fileName, String fileExtension, String data) {
        path += File.separator + fileName + "." + fileExtension;
        File file = new File(path);
        if (!file.exists()) {
            try {
                File directory = new File(file.getParent());
                if (!directory.exists()) {
                        directory.mkdirs();
                }
                file.createNewFile();
            } catch (IOException e) {
                log("Excepton Occured: " + e.toString());
            }
        }

        try {
            // Convenience class for writing character files
            FileWriter writer;
            writer = new FileWriter(file.getAbsoluteFile(), true);

            // Writes text to a character-output stream
            BufferedWriter bufferWriter = new BufferedWriter(writer);
            bufferWriter.write(data.toString());
            bufferWriter.close();

            log("Company data saved at file location: " + path + " Data: " + data + "\n");
        } catch (IOException e) {
            log("Hmm.. Got an error while saving Company data to file " + e.toString());
        }
    }
    
    private static void log(String string) {
        System.out.println(string);
    }
    
}
