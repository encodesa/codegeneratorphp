/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generadorcodigophp;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabrielguevara
 */
public class DTOGenerator {
    
    private String template;
    private String className;
    private String properties;
    private String getters;
    private String setters;
    private String setDataFromArrayBody = "";
    private List<String> propertiesList = new ArrayList<>();
    
    private String setDataFromArrayLineTemplate = "if (array_key_exists(CamposApis::API_%tabla%_CAMPO_%campo%, $arrayEntity)) \n" +
"                \t$this->_%propiedad% = $arrayEntity[CamposApis::API_%tabla%_CAMPO_%campo%];";

    public DTOGenerator() {
        this.template = "<?php\n" +
                "\n" +
                "/* \n" +
                " * To change this license header, choose License Headers in Project Properties.\n" +
                " * To change this template file, choose Tools | Templates\n" +
                " * and open the template in the editor.\n" +
                " */\n" + 
                "\n"+
                "class %classname% implements DTO {\n" +
                "\n" + 
                "%variablesglobales%\n" + 
                "\n" + 
                "\tpublic function setDataFromArray($arrayEntity) {\n" +
                "\t\t%asignacionvaribalefromarray%\n" +
                "\t}\n" + 
                "\n" + 
                "%getters%\n" + 
                "\n" +
                "%setters%\n" + 
                "\n" +
                "}";
        this.className = "";
        this.properties = "";
        this.getters = "";
        this.setters = "";
    }
    
    public void setClassName(String className) {
        this.className = PropertieTranslator.createUpperCamelCase(className) + "DTO";
    }
    
    public void appendProperties(List<String> properties) {
        String[] props = properties.toArray(new String[0]);
        appendProperties(props);
    }
    
    public void appendProperties(String[] properties) {
        for (String propertie : properties) {
            appendPropertie(propertie);
        }
    }
    
    public void appendPropertie(String propertie) {
        String propertieName = PropertieTranslator.createLowerCamelCase(propertie);
        String newPropertie = "\tprivate $_" + propertieName + ";";
        if (this.properties.isEmpty()) {
            this.properties = newPropertie;
        } else {
            this.properties += "\n" + newPropertie;
        }
        this.propertiesList.add(propertieName);
    }
    
    public void generateGetters() {
        String getters = "";
        for (String prop : propertiesList) {
            char first = Character.toUpperCase(prop.charAt(0));
            String propertie = first + prop.substring(1);
            String getter = "\tfunction get" + propertie + "() {\n" + "\t\treturn $this->_" + prop + ";\n\t}";
            if(getters.isEmpty()) {
                getters = getter;
            } else {
                getters += "\n\n" + getter;
            }
        }
        this.getters = getters;
    }
    
    public void generateSetters() {
        String setters = "";
        for (String prop : propertiesList) {
            char first = Character.toUpperCase(prop.charAt(0));
            String propertie = first + prop.substring(1);
            String setter = "\tfunction set" + propertie + "($_"+prop+") {\n" + "\t\t$this->_" + prop + " = $_"+ prop +";\n\t}";
            if(setters.isEmpty()) {
                setters = setter;
            } else {
                setters += "\n\n" + setter;
            }
        }
        this.setters = setters;
    }
    
    public void replaceValues() {
        template = template.replace("%classname%", className);
        template = template.replace("%variablesglobales%", properties);
        template = template.replace("%getters%", getters);
        template = template.replace("%setters%", setters);
        template = template.replace("%asignacionvaribalefromarray%", setDataFromArrayBody);
    }
    
    public void printGeneratedClass() {
        System.out.println(template);
    }
    
    public void generateSetDataFromArrayBody(String tabla, List<String> campos) {
        String[] fields = campos.toArray(new String[0]);
        generateSetDataFromArrayBody(tabla, fields);
    }
    
    public void generateSetDataFromArrayBody(String tabla, String[] campos) {
        setDataFromArrayBody = "\n";
        for(String campo : campos) {
            String line = "\t\t"+setDataFromArrayLineTemplate.replaceAll("%tabla%", tabla.toUpperCase()).
                    replaceAll("%campo%", campo.toUpperCase()).
                    replaceAll("%propiedad%", PropertieTranslator.createLowerCamelCase(campo));
            setDataFromArrayBody += line + "\n";
        }
    }
    
    public String generateDTOClass(String tabla, List<String> campos) {
        String[] fields = campos.toArray(new String[0]);
        return generateDTOClass(tabla, fields);
    }
    
    public String generateDTOClass(String tabla, String[] campos) {
        this.setClassName(tabla);
        this.appendProperties(campos);
        this.generateGetters();
        this.generateSetters();
        this.generateSetDataFromArrayBody(tabla, campos);
        this.replaceValues();
        return this.template;
    }

    public String getClassName() {
        return className;
    }
    
}
