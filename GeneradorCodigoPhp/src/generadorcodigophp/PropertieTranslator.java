/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generadorcodigophp;

/**
 *
 * @author gabrielguevara
 */
public class PropertieTranslator {
    
    public static String createUpperCamelCase(String element) {
        String[] componentsOfName = element.contains("_") ? element.split("_") : new String[]{element};
        String finalElement = "";
        for(String component : componentsOfName) {
            char first = Character.toUpperCase(component.charAt(0));
            component = first + component.substring(1);
            finalElement += component;
        }
        element = finalElement;
        return element;
    }
    
    public static String createLowerCamelCase(String element) {
        if (element.contains("_")) {
            String[] componentsOfName = element.split("_");
            String finalElement = "";
            boolean isFirstComponent = true;
            for(String component : componentsOfName) {
                if (!isFirstComponent) {
                    char first = Character.toUpperCase(component.charAt(0));
                    component = first + component.substring(1);
                }
                finalElement += component;
                isFirstComponent = false;
            }
            element = finalElement;
        }
        return element;
    }
    
}
