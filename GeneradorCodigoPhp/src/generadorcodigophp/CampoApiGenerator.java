/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generadorcodigophp;

import java.util.List;
import java.util.Map;

/**
 *
 * @author gabrielguevara
 */
public class CampoApiGenerator {
    
    private String contenido = "";
    
    private String comentBlockTemplate = "\t//Nombre de los campos de la API %apiuppercamel%";
    private String lineTemplate = "\tconst API_%apicapital%_CAMPO_%campocapital% = '%campolowercamel%';";
    
    private String template = "<?php\n" +
"\n" +
"/* \n" +
" * To change this license header, choose License Headers in Project Properties.\n" +
" * To change this template file, choose Tools | Templates\n" +
" * and open the template in the editor.\n" +
" */\n" +
"abstract class CamposApis {\n" +
"    \n" +
"\t/*****************************************************\n" +
"\t* Nomenclatura: API_NOMBREAPI_CAMPO_NOMBRECAMPO *\n" +
"\t*****************************************************/\n" +
"    \n" +
"%contenido%\n"
            + "}";
    
    public void addDefinition(String api, List<String> campos) {
        String[] fields = campos.toArray(new String[0]);
        addDefinition(api, fields);
    }
    
    public void generarContenidoDesdeLista(Map<String, List<String>> contenido) {
        for ( String api : contenido.keySet() ) {
            addDefinition(api, contenido.get(api));
        }
    }
    
    public void generarContenido(Map<String, String[]> contenido) {
        for ( String api : contenido.keySet() ) {
            addDefinition(api, contenido.get(api));
        }
    }
    
    public void addDefinition(String api, String[] campos) {
        String apiCapital = api.toUpperCase();
        String apiUpperCamel = PropertieTranslator.createUpperCamelCase(api);
        String comentario = comentBlockTemplate.replace("%apiuppercamel%", apiUpperCamel);
        contenido += "\n" + comentario;
        for (String campo : campos) {
            String campoCapital = campo.toUpperCase();
            String campoLowerCamel = PropertieTranslator.createLowerCamelCase(campo);
            String newLine = lineTemplate.replace("%apicapital%", apiCapital).replace("%campocapital%", campoCapital).replace("%campolowercamel%", campoLowerCamel);
            contenido += "\n" + newLine;
        }
        contenido += "\n";
    }
    
    public String generateClass() {
        return template.replace("%contenido%", contenido);
    }
    
}
