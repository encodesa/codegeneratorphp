/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generadorcodigophp;

import java.util.List;

/**
 *
 * @author gabrielguevara
 */
public class DAOGenerator {
    
    private String className;
    private String classNameCapital;
    private String classNameUpperCamel;
    private String classNameLowerCamel;
    private String columnsName;
    private String columnsInsertValues;
    private String columnsUpdateValues;
    private String insertParamsType;
    private String updateParamsType;
    
    private String columnsValuesLine = "$%tablalowercamel%->get%columnuppercamel%()";
    private String columnsNameLine = "CamposTablas::TABLA_%tablacapital%_CAMPO_%campocapital%.' as '.CamposApis::API_%tablacapital%_CAMPO_%campocapital%";
    private String template = "<?php\n" +
"\n" +
"/* \n" +
" * To change this license header, choose License Headers in Project Properties.\n" +
" * To change this template file, choose Tools | Templates\n" +
" * and open the template in the editor.\n" +
" */\n" +
"class %classname% extends DAO {\n" +
"    \n" +
"    function __construct() {\n" +
"        parent::__construct();\n" +
"        \n" +
"        $this->_tableName= NombreTabla::TABLA_%tablacapital%;\n" +
"        $columnsName=array(\n" +
"        %columnsname%\n" +
"                );\n" +
"        \n" +
"        parent::generarColumsName($columnsName);\n" +
"    }\n" +
"    \n" +
"    function insertar%tablauppercamel%(%tablauppercamel%DTO $%tablalowercamel%) {\n" +
"        $this->_columnsValues = array( \n" +
"            %columnsvaluesinsert%\n" +
"        );\n" +
"        $this->paramsTipeDescription = '%paramstypeinsert%';\n" +
"        parent::insert($%tablalowercamel%);\n" +
"        return $this->last_id;\n" +
"    }\n" +
"    \n" +
"    function actualizar%tablauppercamel%(%tablauppercamel%DTO $%tablalowercamel%, $id) {\n" +
"        $this->_columnsValues = array( \n" +
"            %columnsvaluesupdate%\n" +
"        );\n" +
"        $this->paramsTipeDescription = '%paramstypeupdate%';\n" +
"        unset($this->_columnsName[0]);\n" +
"        parent::update($%tablalowercamel%, $id);\n" +
"    }\n" +
"\n" +
"    function find%tablauppercamel%ById($id) {\n" +
"        $%tablalowercamel% =  parent::findById($id);\n" +
"        if ( $%tablalowercamel% != null && count($%tablalowercamel%) > 0 ) {\n" +
"            return $%tablalowercamel%[0];\n" +
"        }\n" +
"        return array();\n" +
"    }\n" +
"    \n" +
"    function find%tablauppercamel%All() {\n" +
"        $%tablalowercamel% =  parent::findAll();\n" +
"        if ( $%tablalowercamel% != null && count($%tablalowercamel%) > 0 ) {\n" +
"            return $%tablalowercamel%;\n" +
"        }\n" +
"        return array();\n" +
"    }\n" +
"    \n" +
"}";
    
    public void setClassName(String className) {
        this.className = PropertieTranslator.createUpperCamelCase(className) + "DAO";
        this.classNameUpperCamel = PropertieTranslator.createUpperCamelCase(className);
        this.classNameLowerCamel = PropertieTranslator.createLowerCamelCase(className);
        this.classNameCapital = className.toUpperCase();
    }
    
    public void generateColumnsName(List<String> columnsName) {
        String[] columns = columnsName.toArray(new String[0]);
        generateColumnsName(columns);
    }
    
    public void generateColumnsName(String[] columnsName) {
        String columns = "";
        boolean isFirst = true;
        for (String column : columnsName) {
            String newLine = columnsNameLine.replaceAll("%tablacapital%", classNameCapital).replaceAll("%campocapital%", column.toUpperCase());
            if( isFirst ) {
                columns = "\t" + newLine;
            } else {
                columns += ", \n\t\t" + newLine;
            }
            isFirst = false;
        }
        this.columnsName = columns;
    }
    
    public void generateInsertValues(List<String> columnsValues, String paramsType) {
        String[] columns = columnsValues.toArray(new String[0]);
        generateInsertValues(columns, paramsType);
    }
    
    public void generateInsertValues(String[] columnsValues, String paramsType) {
        String values = "";
        boolean isFirst = true;
        for (String columnValue : columnsValues) {
            String newLine = columnsValuesLine.replaceAll("%tablalowercamel%", classNameLowerCamel).replaceAll("%columnuppercamel%", PropertieTranslator.createUpperCamelCase(columnValue));
            if( isFirst ) {
                values = "\t" + newLine;
            } else {
                values += ", \n\t\t" + newLine;
            }
            isFirst = false;
        }
        this.columnsInsertValues = values;
        this.insertParamsType = paramsType;
    }
    
    public void generateUpdateValues(List<String> columnsValues, String paramsType) {
        String[] columns = columnsValues.toArray(new String[0]);
        generateUpdateValues(columns, paramsType);
    }
    
    public void generateUpdateValues(String[] columnsValues, String paramsType) {
        String values = "";
        boolean isFirst = true;
        for (String columnValue : columnsValues) {
            String newLine = columnsValuesLine.replaceAll("%tablalowercamel%", classNameLowerCamel).replaceAll("%columnuppercamel%", PropertieTranslator.createUpperCamelCase(columnValue));
            if( isFirst ) {
                values = "\t" + newLine;
            } else {
                values += ", \n\t\t" + newLine;
            }
            isFirst = false;
        }
        this.columnsUpdateValues = values;
        this.updateParamsType = paramsType;
    }
    
    public String replaceValues() {
        template = template.replace("%classname%", className);
        template = template.replaceAll("%tablauppercamel%", classNameUpperCamel);
        template = template.replaceAll("%tablalowercamel%", classNameLowerCamel);
        template = template.replaceAll("%tablacapital%", classNameCapital);
        template = template.replaceAll("%columnsname%", columnsName);
        template = template.replace("%columnsvaluesinsert%", columnsInsertValues);
        template = template.replace("%paramstypeinsert%", insertParamsType);
        template = template.replace("%columnsvaluesupdate%", columnsUpdateValues);
        template = template.replace("%paramstypeupdate%", updateParamsType);
        return template;
    }
    
    public String generateDAOClass(String tabla, List<String> camposInsert, String paramsTypeInsert, List<String> camposUpdate, String ParamsTypeUpdate) {
        String[] camposInsertArr = camposInsert.toArray(new String[0]);
        String[] camposUpdateArr = camposUpdate.toArray(new String[0]);
        return generateDAOClass(tabla, camposInsertArr, paramsTypeInsert, camposUpdateArr, ParamsTypeUpdate);
    }
    
    public String generateDAOClass(String tabla, String[] camposInsert, String paramsTypeInsert, String[] camposUpdate, String ParamsTypeUpdate) {
        this.setClassName(tabla);
        this.generateColumnsName(camposInsert);
        this.generateInsertValues(camposInsert, paramsTypeInsert);
        this.generateUpdateValues(camposUpdate, ParamsTypeUpdate);
        return this.replaceValues();
    }

    public String getClassName() {
        return className;
    }
    
}
