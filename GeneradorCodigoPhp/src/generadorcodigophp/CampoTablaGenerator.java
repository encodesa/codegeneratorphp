/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generadorcodigophp;

import java.util.List;
import java.util.Map;

/**
 *
 * @author gabrielguevara
 */
public class CampoTablaGenerator {
    
    private String contenido = "";
    
    private String comentBlockTemplate = "\t//Nombre de los campos de la TABLA %tabla%";
    private String lineTemplate = "\tconst TABLA_%tablacapital%_CAMPO_%campocapital% = '%campo%';";
    
    private String template = "<?php\n" +
"\n" +
"/* \n" +
" * To change this license header, choose License Headers in Project Properties.\n" +
" * To change this template file, choose Tools | Templates\n" +
" * and open the template in the editor.\n" +
" */\n" +
"abstract class CamposTablas {\n" +
"    \n" +
"\t/*****************************************************\n" +
"\t* Nomenclatura: TABLA_NOMBRETABLA_CAMPO_NOMBRECAMPO *\n" +
"\t*****************************************************/\n" +
"    \n" +
"%contenido%\n"
            + "}";
    
    public void addDefinition(String tabla, List<String> campos) {
        String[] fields = campos.toArray(new String[0]);
        addDefinition(tabla, fields);
    }
    
    public void generarContenidoDesdeLista(Map<String, List<String>> contenido) {
        for ( String tabla : contenido.keySet() ) {
            addDefinition(tabla, contenido.get(tabla));
        }
    }
    
    public void generarContenido(Map<String, String[]> contenido) {
        for ( String tabla : contenido.keySet() ) {
            addDefinition(tabla, contenido.get(tabla));
        }
    }
    
    public void addDefinition(String tabla, String[] campos) {
        String tablaCapital = tabla.toUpperCase();
        String tablaUpperCamel = PropertieTranslator.createUpperCamelCase(tabla);
        String comentario = comentBlockTemplate.replace("%tabla%", tabla);
        contenido += "\n" + comentario;
        for (String campo : campos) {
            String campoCapital = campo.toUpperCase();
            String newLine = lineTemplate.replace("%tablacapital%", tablaCapital).replace("%campocapital%", campoCapital).replace("%campo%", campo);
            contenido += "\n" + newLine;
        }
        contenido += "\n";
    }
    
    public String generateClass() {
        return template.replace("%contenido%", contenido);
    }
    
}
