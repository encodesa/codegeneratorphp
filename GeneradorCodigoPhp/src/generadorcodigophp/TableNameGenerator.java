/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generadorcodigophp;

import java.util.List;
import java.util.Map;

/**
 *
 * @author gabrielguevara
 */
public class TableNameGenerator {
    
    private String contenido = "";
    
    private String lineTemplate = "\tconst TABLA_%tablacapital% = '%tabla%';";
    
    private String template = "<?php\n" +
"\n" +
"/*\n" +
" * To change this license header, choose License Headers in Project Properties.\n" +
" * To change this template file, choose Tools | Templates\n" +
" * and open the template in the editor.\n" +
" */\n" +
"\n" +
"/**\n" +
" * Description of NombreTabla\n" +
" *\n" +
" * @author ENCODE S.A.\n" +
" */\n" +
"abstract class NombreTabla {\n" +
"    \n" +
"    /*****************************************************\n" +
"     * Nomenclatura: TABLA_NOMBRETABLE                   *\n" +
"     *****************************************************/\n" +
"    %contenido%\n" +
"    \n" +
"}";
    
    public void generarContenidoDesdeLista(List<String> tablas) {
        String[] tables = tablas.toArray(new String[0]);
        generarContenido(tables);
    }
    
    public void generarContenido(String[] tablas) {
        for ( String tabla : tablas ) {
            addDefinition(tabla);
        }
    }
    
    public void addDefinition(String tabla) {
        String tablaCapital = tabla.toUpperCase();
        String tablaUpperCamel = PropertieTranslator.createUpperCamelCase(tabla);
        String newLine = lineTemplate.replace("%tablacapital%", tablaCapital).replace("%tabla%", tabla);
        contenido += "\n" + newLine;
    }
    
    public String generateClass() {
        return template.replace("%contenido%", contenido);
    }
    
}