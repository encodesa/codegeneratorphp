/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generadorcodigophp;

/**
 *
 * @author gabrielguevara
 */
public class ControllerGenerator {
    
    private String className = "";
    private String classNameUpperCamel = "";
    private String classNameLowerCamel = "";
    private String classNameCapital = "";
    private String template = "<?php\n" +
"\n" +
"/* \n" +
" * To change this license header, choose License Headers in Project Properties.\n" +
" * To change this template file, choose Tools | Templates\n" +
" * and open the template in the editor.\n" +
" */\n" +
"\n" +
"require $_RUTA_SERVICIOS . $_PAGINA . 'Services.php';\n" +
"require $_RUTA_DTO . $_PAGINA . 'DTO.php';\n" +
"require $_RUTA_DAO  . $_PAGINA .  'DAO.php';\n" +
"\n" +
"$%tablalowercamel% = new %tablauppercamel%();";
    
    public void setClassName(String className) {
        this.className = PropertieTranslator.createUpperCamelCase(className) + "Controller";
        this.classNameUpperCamel = PropertieTranslator.createUpperCamelCase(className);
        this.classNameLowerCamel = PropertieTranslator.createLowerCamelCase(className);
        this.classNameCapital = className.toUpperCase();
    }
    
    public String generateController() {
        return template.replace("%tablalowercamel%", classNameLowerCamel).replace("%tablauppercamel%", classNameUpperCamel);
    }

    public String getClassName() {
        return className;
    }
    
}
