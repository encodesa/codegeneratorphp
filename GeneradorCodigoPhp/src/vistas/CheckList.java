/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author gabrielguevara
 */
public class CheckList extends JList<CheckList.CheckableItem> {
    String[] contenido = { "swing", "home", "basic", "metal", "JList" };

    final JList list = new JList(createData(contenido));

    public JList getList() {
        return list;
    }
    
    public CheckList() {
        list.setCellRenderer(new CheckListRenderer());
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setBorder(new EmptyBorder(0, 4, 0, 0));
        list.addMouseListener(new MouseAdapter() {
          public void mouseClicked(MouseEvent e) {
            int index = list.locationToIndex(e.getPoint());
            CheckableItem item = (CheckableItem) list.getModel()
                .getElementAt(index);
            item.setSelected(!item.isSelected());
            Rectangle rect = list.getCellBounds(index, index);
            list.repaint(rect);
          }
        });
        JScrollPane sp = new JScrollPane(list);
    }

  private CheckableItem[] createData(String[] strs) {
    int n = strs.length;
    CheckableItem[] items = new CheckableItem[n];
    for (int i = 0; i < n; i++) {
      items[i] = new CheckableItem(strs[i]);
    }
    return items;
  }

  class CheckableItem {
    private String str;

    private boolean isSelected;

    public CheckableItem(String str) {
      this.str = str;
      isSelected = false;
    }

    public void setSelected(boolean b) {
      isSelected = b;
    }

    public boolean isSelected() {
      return isSelected;
    }

    public String toString() {
      return str;
    }
  }
  
  class CheckListRenderer extends JCheckBox implements ListCellRenderer {

    public CheckListRenderer() {
      setBackground(UIManager.getColor("List.textBackground"));
      setForeground(UIManager.getColor("List.textForeground"));
    }

    public Component getListCellRendererComponent(JList list, Object value,
        int index, boolean isSelected, boolean hasFocus) {
      setEnabled(list.isEnabled());
      setSelected(((CheckableItem) value).isSelected());
      setFont(list.getFont());
      setText(value.toString());
      return this;
    }
  }
  
  
  
}